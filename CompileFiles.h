#pragma once

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <process.h>
#include <iostream>
#include <shlobj.h>  
#include <windows.h>
#include <algorithm>
#include <string>
#include <fstream>
#include "ParseFiles.h"

void comp(char* path);

void BrowseFolder(void);

void SearchFolder(TCHAR* path);

std::string ReplaceAll(std::string str, const std::string& from, const std::string& to);