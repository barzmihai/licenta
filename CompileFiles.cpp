#include "CompileFiles.h"
#include "distorm.h"
#include <string>
#include "wstring.h"

using namespace std;

void comp(char* path) {

    FILE* bat = fopen("D:\\licenta\\Plagiarism\\CompilingCommands.bat", "w");
    fprintf(bat, "\"C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\Common7\\Tools\\VsDevCmd.bat\" ");
    fprintf(bat, path);
    fclose(bat);
    system("D:\\licenta\\Plagiarism\\CompilingCommands.bat");
}

//BROWSE FOLDER - Opens a browse folder dialog.
void BrowseFolder(void)
{
    TCHAR path[MAX_PATH];
    BROWSEINFO bi = { 0 };
    bi.lpszTitle = ("All Folders Automatically Recursed.");
    LPITEMIDLIST pidl = SHBrowseForFolder(&bi);

    if (pidl != 0)
    {
        // get the name of the folder and put it in path
        SHGetPathFromIDList(pidl, path);

        //Set the current directory to path
        SetCurrentDirectory(path);

        //Begin the search
        SearchFolder(path);

        // free memory used
        IMalloc* imalloc = 0;
        if (SUCCEEDED(SHGetMalloc(&imalloc)))
        {
            imalloc->Free(pidl);
            imalloc->Release();
        }
    }
}//BROWSE FOLDER

//SEARCH FOLDER - Searches folder and all sub-folders, 
//reading every file it comes across.
void SearchFolder( TCHAR * path ) 
{     
    //Declare all needed handles     
    WIN32_FIND_DATA FindFileData;     
    HANDLE hFind;     
    TCHAR filename[ MAX_PATH + 256 ];     
    TCHAR pathbak[ MAX_PATH ];   
    char command[512] = "";
    string parse;
    int i = 0;

    //Make a backup of the directory the user chose         
    strcpy_s( pathbak, path );

    //Find the first file in the directory the user chose     
    hFind = FindFirstFile ( "*.cpp", &FindFileData );

    //Use a do/while so we process whatever FindFirstFile returned     
    do     
    {        
        
        //Is it valid?         
        if ( hFind != INVALID_HANDLE_VALUE )         
        {             
            //Is it a . or .. directory? If it is, skip, or we'll go forever.             
            if ( ! ( strcmp( FindFileData.cFileName, "." ) ) || 
                ! ( strcmp( FindFileData.cFileName, ".." ) ) )             
            {                 
                continue;             
            }             
            //Restore the original directory chosen by the user             
            strcpy( path, pathbak );

            //Append the file found on to the path of the 
            //directory the user chose             
            sprintf( path, "%s\\%s", path, FindFileData.cFileName );

            //If SetCurrentDirectory Succeeds ( returns 1 ), the 
            //current file is a directory. Pause this function,             
            //and have it call itself. This will begin the whole 
            //process over in a sub directory.             
            if ( ( SetCurrentDirectory( path ) ) )             
            {                 
                SearchFolder( path );             
            } 

            //Otherwise right here is where you need to insert what you want to do.             
            //As an example, let's add the filename to a list box.             
            //INSERT WHAT YOU WANT DONE BELOW!             
            //SendMessage( m_listbox_hwnd, LB_ADDSTRING, 0, path );
            string pathexe = path;
            pathexe.replace(pathexe.length()-4, 4, ".exe");
            parse.append(pathexe);
            parse.append("@");
            i++;
            strcat_s(command, " & cl /EHsc /Od /MD ");
            strcat_s(command, path);
            
        }     
    }    
    while ( FindNextFile ( hFind, &FindFileData ) 
        && hFind != INVALID_HANDLE_VALUE );     
    FindClose ( hFind );
    comp(command);
    Parse(parse, i);
    
}//SEARCH FOLDER
